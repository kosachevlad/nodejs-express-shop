const keys = require('../keys')

module.exports = function(email, token) {
  return {
    to: email,
    from: keys.EMAIL_FROM,
    subject: 'Password reset',
    html: `
      <h1> You forgot a password? </h1>
      <p> Ignore if no </p>
      <p> Or click a link below </p>
      <p> <a href="${keys.BASE_URL}/auth/password/${token}">Reset a pass</a> </p>
      <hr />
      <a href="${keys.BASE_URL}">Courses shop</a>
    `
  }
}